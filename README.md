# Backend for test task for NL
> made by [@coma8765](https://gitlab.com/coma8765)


## Pages
- `/api/openapi` - documentation
- `/api/openapi.json` - openapi.json swagger docs

## Dependencies

- python3.11+
- pipenv (install via `pip install --upgrade pipenv`)

## Startup guides

### Docker Compose
#### Build
```shell
docker compose -f templates/docker-compose.yaml build
```

#### Startup
```shell
docker compose \
  -f templates/docker-compose.yaml \
   up -d
```

#### Migrate
```shell
docker compose \
  -f templates/docker-compose.yaml \
   exec -it api pipenv run python -m src.db.sqlalchemy
```

### Docker
#### Build
```shell
docker build -f templates/Dockerfile -t nl-api .
```

#### Run
```shell
docker run \
  -p 8000:8000 \
  -e JWT_SECRET=someJwtToken \
  -e POSTGRES_DNS=postgres://postgres:password@127.0.0.1:5432/database \
  nl-api
```

### Production

#### Install dependence

```shell
make install 
```

#### Run project
**Copy and fill .env.example to .env**
```shell
make run
```
_Look at http://127.0.0.1:8000_

### Development

#### Install dependence

```shell
make install-dev
```

#### Run project

**Copy and fill .env.example to .env**
```shell
make dev
```
_Look at http://127.0.0.1:8000_


## Migrate database
_After install depends or from docker_
```shell
make migrate
```
