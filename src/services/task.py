"""Task use case"""
import uuid
from abc import ABC, abstractmethod
from functools import lru_cache

from fastapi import Depends

from src.api.v1.user_utils import get_user
from src.core import exc
from src.models.dto import List
from src.models.task import Task, TaskFilter, TaskRef, TaskUpd
from src.models.user import User


class TaskRepository(ABC):
    """Task repository"""

    @abstractmethod
    async def get_by_id(self, task_id: uuid.UUID) -> Task:
        """Returns task by ID using persistence

        Returns:
            Task: Task with some ID

        Raises:
             NotFound: If task with some ID not found
        """

    @abstractmethod
    async def get_user_notes(
        self,
        user_id: uuid.UUID,
        page: int = 1,
        params: TaskFilter | None = None,
    ) -> List[Task]:
        """Returns tasks by owner ID using persistence

        Returns:
            List[Task]: List DTO with tasks
        """

    @abstractmethod
    async def save(self, ref: TaskRef) -> None:
        """Saves task with some ref using persistence"""

    @abstractmethod
    async def update(self, task_id: uuid.UUID, ref: TaskUpd) -> None:
        """Updates task using persistence

        Raises:
            NotFound: If task not found
        """


class TaskService:
    """Task service"""

    def __init__(
        self,
        repo: TaskRepository = Depends(),
        user: User = Depends(get_user),
    ):
        self.user = user
        self.repo = repo

    async def get_by_id(self, task_id: uuid.UUID) -> Task:
        """Returns task by ID

        Returns:
            Task: Task with some ID

        Raises:
             NotFound: If task with some ID not found or forbidden
        """
        task = await self.repo.get_by_id(task_id)

        if task.user_id != self.user.id:
            raise exc.NotFound

        return task

    async def get_user_notes(
        self,
        page: int = 1,
        params: TaskFilter | None = None,
    ) -> List[Task]:
        """Returns tasks by owner ID

        Args:
            params: Task's filter
            page: Lookup page

        Returns:
            List[Task]: Paginated list with tasks
        """
        return await self.repo.get_user_notes(self.user.id, page, params)

    async def create(self, ref: TaskRef) -> Task:
        """Creates task with some ref

        Args:
            ref: Reference for Task

        Returns:
            Task: Created task
        """
        task = ref.create(self.user.id)
        await self.repo.save(task)
        return task

    async def update(self, task_id: uuid.UUID, ref: TaskUpd) -> None:
        """Updates task

        Raises:
            Forbidden: If user not owner task
        """
        task = await self.get_by_id(task_id)

        if task.user_id == self.user.id:
            await self.repo.update(task_id, ref)
        else:
            raise exc.Forbidden()


@lru_cache()
def get_notes_service(repo: TaskRepository = Depends()) -> TaskService:
    """Returns note service

    Returns:
        TaskService: Task use case skeleton
    """
    return TaskService(repo)
