"""User use case"""
import uuid
from abc import ABC, abstractmethod

from fastapi import Depends

from src.core import exc
from src.models.user import User, UserRef


class LoginNonUnique(exc.BadRequest):
    pass


class UserRepository(ABC):
    """User repository"""

    @abstractmethod
    async def get_by_id(self, user_id: uuid.UUID) -> User:
        """Returns user by ID using persistence

        Args:
            user_id: ID of user

        Returns:
            User with some ID

        Raises:
            NotFount: If user not found
        """

    @abstractmethod
    async def get_by_login(
        self,
        login: str,
    ) -> User:
        """Returns user by login using persistence"""

    @abstractmethod
    async def save(self, user_ref: User) -> None:
        """Save user to persistence"""


class UserService:
    """User service"""

    __slots__ = ("repo",)

    def __init__(self, repo: UserRepository = Depends()):
        self.repo = repo

    async def create(
        self,
        user_ref: UserRef,
        hashed_password: str,
    ) -> User:
        """Creates user

        Args:
            user_ref: User reference
            hashed_password: User's hashed password

        Returns:
            User model

        Raises:
            LoginNonUnique: Raises when user's login already exist in persistent
        """
        user = user_ref.full
        user.hashed_password = hashed_password
        await self.repo.save(user)

        return user

    async def get_by_login(
        self,
        login: str,
    ) -> User:
        """Returns user by login

        Args:
            login: User's login

        Returns:
            Hashed password

        Raises:
            NotFound: If user not found
        """
        return await self.repo.get_by_login(login)

    async def get_by_id(self, user_id: uuid.UUID) -> User:
        """Returns user by ID

        Args:
            user_id: ID of user

        Returns:
            User with some ID

        Raises:
            NotFount: If user not found
        """
        return await self.repo.get_by_id(user_id)
