"""Token provider"""
from abc import ABC, abstractmethod


class HashProvider(ABC):
    """Hash provider"""

    @abstractmethod
    def hash(self, data: str) -> str:
        """Hash data"""

    @abstractmethod
    def verify(self, data: str, hashed: str) -> bool:
        """Verify hash"""
