"""Token provider"""
from abc import ABC, abstractmethod
from typing import Generic, Literal, TypeVar

from src.core import exc
from src.core.pydantic import BaseModel


class Token(BaseModel):
    """Auth token"""

    token: str
    type: Literal["Bearer"] = "Bearer"


T = TypeVar("T")


class TokenProvider(ABC, Generic[T]):
    """Token provider"""

    @abstractmethod
    def get_data_from_token(self, token: Token) -> T:
        """Get data from token

        Returns:
            Some data from token
        Raises:
            BadRequest: If token invalid
        """

    @staticmethod
    def parse_str_to_token(token_str: str) -> Token:
        """Parses str to token

        Returns:
            Token: Token from string
        """
        if len(token_str.split()) != 2:
            raise exc.BadRequest("invalid token length")

        return Token(
            type=token_str.split()[0],
            token=token_str.split()[1],
        )

    @abstractmethod
    def create_token(self, data: T) -> Token:
        """Create token with data

        Returns:
            Token: Token with some data
        """
