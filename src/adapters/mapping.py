"""Adapter overrides"""
from abc import ABC
from typing import Any, Callable, Type

from src.adapters.providers.hash import HashProvider
from src.adapters.providers.token import TokenProvider
from src.adapters.third_party.bcrypt import BcryptHash
from src.adapters.third_party.jose import JoseToken

ADAPTER_OVERRIDES: dict[
    Type[ABC] | Callable[..., Any], Type[ABC] | Callable[..., Any]
] = {
    TokenProvider: JoseToken,
    HashProvider: BcryptHash,
}
