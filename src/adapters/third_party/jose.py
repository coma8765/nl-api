"""Jose JWT implementation"""
import logging
from datetime import datetime, timedelta
from typing import Generic

from jose import JWTError, jwt

from src.adapters.providers.token import T, Token, TokenProvider
from src.core.config import JWT_SECRET
from src.core.exc import BadRequest


class JoseToken(TokenProvider, Generic[T]):
    """Jose token implementation"""

    _logger = logging.getLogger(__name__)
    _algorithm = "HS256"

    def __init__(self, secret: str | None = JWT_SECRET):
        self._secret = secret

        if secret is None:
            self._logger.warning("jwt secret not defined")

    def get_data_from_token(self, token: Token) -> T:
        """Get data from JWT token

        Args:
            token: JWT token

        Returns:
            Data from token

        Raises:
            BadRequest: If token invalid
        """
        if self._secret is None:
            raise BadRequest

        try:
            data = jwt.decode(
                token.token,
                self._secret,
                algorithms=self._algorithm,
            ).get("u", None)

            if data is None:
                raise BadRequest

            return data
        except JWTError as exc:
            raise BadRequest from exc

    def create_token(self, data: T) -> Token:
        """Create JWT token

        Args:
            data: some data

        Returns:
            token with type
        """
        if self._secret is None:
            raise BadRequest

        token = jwt.encode(
            {
                "exp": datetime.utcnow() + timedelta(days=30),
                "u": data,
            },
            self._secret,
            self._algorithm,
        )

        return Token(token=token)
