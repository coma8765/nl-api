"""Bcrypt JWT implementation"""
from functools import lru_cache

from passlib.handlers.bcrypt import bcrypt

from src.adapters.providers.hash import HashProvider
from src.core.config import HASH_SALT


class BcryptHash(HashProvider):
    """Bcrypt hash"""

    def __init__(self, salt: str | None = HASH_SALT):
        self._salt = salt
        # TODO(bcrypt): Add salt for hashing

    def hash(self, data: str) -> str:
        """Bcrypt create hash

        Returns:
            Hashed string
        """
        return bcrypt.hash(data)

    def verify(self, data: str, hashed: str) -> bool:
        """Bcrypt verify hash

        Returns:
            bool: Does equal data
        """
        return bcrypt.verify(data, hashed)


@lru_cache()
def get_bcrypt_hash(salt: str | None) -> BcryptHash:
    """Get bcrypt hash class

    Returns:
        BcryptHash: BcryptHash instance
    """
    return BcryptHash(salt)
