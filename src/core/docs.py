"""Protected docs"""
import base64
from typing import Any

from fastapi import FastAPI, Request, Response
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.responses import HTMLResponse
from starlette.staticfiles import StaticFiles

from src.core.exc import UnauthorizedException


def add_protected_docs(
    app: FastAPI,
    users_and_passwords: list[tuple[str, str]],
) -> None:
    """Setup documentation via auth"""

    def auth(token: str | None) -> None:
        """Check auth for access to docs"""
        if len(users_and_passwords) == 0:
            return

        if token is None:
            raise UnauthorizedException()

        user, password = base64.b64decode(token.split()[1]).decode().split(":")

        if not next(
            filter(
                lambda x: x[0] == user and x[1] == password,
                users_and_passwords,
            ),
            None,
        ):
            raise UnauthorizedException()

    app.mount(
        "/api/openapi-static",
        StaticFiles(directory="static"),
        name="static",
    )

    @app.get("/api/openapi.json", include_in_schema=False)
    def openapi(request: Request) -> dict[str, Any]:
        """OpenAPI scheme

        Args:
            request: HTTP request

        Returns:
            OpenAPI documentation
        """

        auth(request.headers.get("authorization", None))

        return app.openapi()

    @app.get("/api/openapi", include_in_schema=False, response_model=None)
    def docs_openapi(
        request: Request,
        response: Response,
    ) -> HTMLResponse | None:
        """OpenAPI docs UI

        Args:
            request: HTTP request
            response: HTTP response

        Returns:
            HTMLResponse: Swagger page
            None: If user not auth
        """
        try:
            auth(request.headers.get("authorization", None))
        except UnauthorizedException:
            response.headers.append("WWW-Authenticate", "Basic")

            response.status_code = 401
            return None

        return get_swagger_ui_html(
            openapi_url="./openapi.json",
            title=app.title + " - Swagger UI",
            swagger_js_url="/api/openapi-static/swagger-ui-bundle.js",
            swagger_css_url="/api/openapi-static/swagger-ui.css",
        )
