"""Application config"""
import logging.config
import os
import string

import yarl as _yarl
from environs import Env as _Env

from src.core.logging import LOGGING

_env = _Env()
_env.read_env()

# logging.config.dictConfig(LOGGING)

PROJECT_NAME = _env.str("PROJECT_NAME", "FastAPI API")

SERVICE_PORT = _env.int("SERVICE_PORT", default=8000)

HASH_SALT = _env.str("HASH_SALT", default="default.salt.a.a.a.a.a")

if len(HASH_SALT) != 22 or any(
    i not in string.ascii_letters and i not in "." for i in HASH_SALT
):
    raise ValueError("HASH_SALT must be 22 char")

JWT_SECRET = _env.str("JWT_SECRET", default="default-secret")
LOG_LEVEL = _env.log_level("LOG_LEVEL", default="DEBUG")

DOCS_AUTH_REFS: list[tuple[str, str]] = [
    (user_and_pass.split("=")[0], user_and_pass.split("=")[1])
    for user_and_pass in _env.list("DOCS_AUTH_REFS", default=[])
    if user_and_pass.split("=") == 2
]

POSTGRES_DNS = _yarl.URL(_env.str("POSTGRES_DNS"))

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
