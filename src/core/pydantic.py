"""Pydantic optimizations configs"""
import orjson
from pydantic import BaseModel as PydanticBaseModel


class BaseModel(PydanticBaseModel):  # pylint: disable=too-few-public-methods
    """Updated BaseModel class"""

    class Config:  # pylint: disable=too-few-public-methods
        """Pydantic configs"""

        json_loads = orjson.loads
        json_dumps = orjson.dumps

        orm_mode = True

        arbitrary_types_allowed = True
