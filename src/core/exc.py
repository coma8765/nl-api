"""Application exc"""


class NotInitException(Exception):
    """Class not init"""


class UnauthorizedException(Exception):
    """User not authorized"""


class Forbidden(Exception):
    """Access denied"""


class IncorrectData(Exception):
    """Incorrect data error"""


class BadRequest(Exception):
    """Bad request data error"""


class NotFound(Exception):
    """Bad request data error"""
