"""Startup application"""
from typing import Any, Callable

import uvicorn
from fastapi import FastAPI
from fastapi.responses import ORJSONResponse
from starlette.middleware.cors import CORSMiddleware

from src.adapters.mapping import ADAPTER_OVERRIDES
from src.api.mapping import API_OVERRIDES
from src.api.v1 import task, user
from src.core.config import DOCS_AUTH_REFS, POSTGRES_DNS, PROJECT_NAME, SERVICE_PORT
from src.core.docs import add_protected_docs
from src.db.mapping import DB_OVERRIDES
from src.db.sqlalchemy import SQLALCHEMY_OVERRIDES

app = FastAPI(
    title=PROJECT_NAME,
    docs_url=None,
    openapi_url=None,
    default_response_class=ORJSONResponse,
)

overrides: tuple[dict[Callable[..., Any], Callable[..., Any]], ...] = (
    ADAPTER_OVERRIDES,
    DB_OVERRIDES,
    SQLALCHEMY_OVERRIDES,
    API_OVERRIDES,
)
for override in overrides:
    # noinspection PyUnresolvedReferences
    app.dependency_overrides.update(override)

add_protected_docs(app, DOCS_AUTH_REFS)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(user.router, prefix="/v1/users", tags=["users"])
app.include_router(task.router, prefix="/v1/tasks", tags=["tasks"])


if __name__ == "__main__":
    uvicorn.run(
        "src.main:app",
        host="0.0.0.0",
        port=SERVICE_PORT,
        reload=True,
    )
