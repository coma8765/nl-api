"""FastAPI user utils"""
import uuid
from http import HTTPStatus

from fastapi import Depends, Header, HTTPException

from src.adapters.providers.token import TokenProvider
from src.models.user import User
from src.services.user import UserService


async def get_user(
    use_case: UserService = Depends(),
    token_: TokenProvider = Depends(),
    authorization: str | None = Header(None),
    token: str | None = None,
) -> User:
    """Returns user by Authorization header

    Returns:
        User if auth correct
    """
    if token is not None:
        token = "Bearer " + token

    authorization = authorization or token
    if authorization is None:
        raise HTTPException(
            detail="you not auth",
            status_code=HTTPStatus.BAD_REQUEST,
        )

    in_token = token_.parse_str_to_token(authorization)
    user_id = token_.get_data_from_token(in_token)

    return await use_case.get_by_id(user_id)
