"""Task routes"""
import uuid
from datetime import datetime
from http import HTTPStatus
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi_utils.cbv import cbv
from pydantic import validator

from src.core import exc
from src.core.pydantic import BaseModel
from src.models.dto import List
from src.models.task import TaskStatus
from src.services import task as s

router = APIRouter()


class TaskRef(BaseModel):
    """Model for create user"""

    title: str
    description: str = ""
    status: TaskStatus = TaskStatus.CREATED
    deadline: datetime | None

    # noinspection PyMethodParameters
    @validator("deadline")
    def __deadline_off_timezone(cls, dt: datetime) -> datetime:
        """Off timezone for deadline

        Returns:
            datetime: Datetime without timezone
        """
        return dt.replace(tzinfo=None)


class TaskUpd(BaseModel):
    """Model for create user"""

    title: str | None = None
    description: str | None = None
    status: TaskStatus | None = None
    deadline: datetime | None

    # noinspection PyMethodParameters
    @validator("deadline")
    def __deadline_off_timezone(cls, dt: datetime | None) -> datetime | None:
        """Off timezone for deadline

        Returns:
            datetime | None: Datetime without timezone or None
        """
        if dt:
            return dt.replace(tzinfo=None)
        return None


class Task(TaskRef):
    """Model for task"""

    id: uuid.UUID

    user_id: uuid.UUID
    created_at: datetime


@cbv(router)
class TaskCBV:
    """Users router"""

    use_case: s.TaskService = Depends()

    @router.post("/")
    async def create_task(self, ref: TaskRef) -> Task:
        """Creates task

        Returns:
            Task: Full task model
        """
        res = await self.use_case.create(s.TaskRef(**ref.dict()))
        return Task(**res.dict())

    @router.get("/user")
    async def user_tasks(
        self,
        status: Annotated[list[TaskStatus] | None, Query()] = None,
        page: int = 1,
    ) -> List[Task]:
        """Returns user's task

        Returns:
            List: List of tasks
        """
        res = await self.use_case.get_user_notes(
            page,
            s.TaskFilter(status=status),
        )

        return List[Task](
            **res.dict(exclude={"list"}),
            list=[Task(**i.dict()) for i in res.list],
        )

    @router.get("/{task_id}")
    async def get_by_id(self, task_id: uuid.UUID) -> Task:
        """Returns user's task

        Returns:
            Task: Task with some ID
        """
        try:
            res = await self.use_case.get_by_id(task_id)
        except exc.NotFound as e:
            raise HTTPException(
                status_code=HTTPStatus.NOT_FOUND,
                detail="task not found",
            ) from e

        return Task(**res.dict())

    @router.patch("/{task_id}", status_code=HTTPStatus.NO_CONTENT)
    async def update(self, task_id: uuid.UUID, ref: TaskUpd) -> None:
        """Updates user"""
        try:
            await self.use_case.update(task_id, s.TaskUpd(**ref.dict()))
        except exc.NotFound as e:
            raise HTTPException(
                status_code=HTTPStatus.NOT_FOUND,
                detail="task not found",
            ) from e
