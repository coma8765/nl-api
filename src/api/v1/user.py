"""Users routes"""
import uuid
from datetime import datetime
from http import HTTPStatus

from fastapi import APIRouter, Depends, HTTPException
from fastapi_utils.cbv import cbv

from src.adapters.providers.hash import HashProvider
from src.adapters.providers.token import Token, TokenProvider
from src.api.v1.user_utils import get_user
from src.core import exc
from src.core.pydantic import BaseModel
from src.services import user as s

router = APIRouter()


class UserRef(BaseModel):
    """Model for create user"""

    login: str

    name: str
    surname: str


class UserAuth(BaseModel):
    """Model with params for auth"""

    login: str
    password: str


class User(UserRef):
    """Model for user"""

    id: uuid.UUID
    created_at: datetime


class UserSignUp(UserRef, UserAuth):
    """Model for SignUp"""


@cbv(router)
class UserCBV:
    """Users router"""

    use_case: s.UserService = Depends()
    token: TokenProvider = Depends()
    hash: HashProvider = Depends()

    @router.post("/signup")
    async def create_user(self, user_ref: UserSignUp) -> User:
        """SignUp for users

        Returns:
            User: full user model
        """
        try:
            res = await self.use_case.create(
                s.UserRef(**user_ref.dict()),
                hashed_password=self.hash.hash(user_ref.password),
            )
            return User(**res.dict())
        except s.LoginNonUnique as e:
            raise HTTPException(
                detail="login already exist",
                status_code=HTTPStatus.BAD_REQUEST,
            ) from e

    @router.post("/login_exists")
    async def login_already_exist_check(self, login: str) -> bool:
        """SignUp for users

        Args:
            login: User's login

        Returns:
            bool: Doesn't exist user with some login
        """
        try:
            await self.use_case.get_by_login(login)
            return True
        except exc.NotFound:
            return False

    @router.post("/signin")
    async def signin(self, ref: UserAuth) -> Token:
        """SignIn for users

        Returns:
            Token: Auth token
        """
        try:
            user = await self.use_case.get_by_login(ref.login)
        except exc.NotFound as e:
            raise HTTPException(
                status_code=HTTPStatus.FORBIDDEN,
                detail="user not found",
            ) from e

        assert isinstance(user.hashed_password, str)
        if self.hash.verify(ref.password, user.hashed_password):
            return self.token.create_token(str(user.id))

        raise HTTPException(
            status_code=HTTPStatus.FORBIDDEN,
            detail="user not found",
        )

    @router.get("/users/{user_id}")
    async def get_user_id(self, user_id: uuid.UUID) -> User:
        """Returns user by ID

        Args:
            user_id: User's ID

        Returns:
            User: User with some ID
        """
        try:
            res = await self.use_case.get_by_id(user_id)
            return User(**res.dict())
        except exc.NotFound as e:
            raise HTTPException(
                detail="user not found",
                status_code=HTTPStatus.NOT_FOUND,
            ) from e

    @router.get("/user")
    async def get_me(self, user: User = Depends(get_user)) -> User:
        """Returns user by ID

        Returns:
            User: Current user
        """
        try:
            res = await self.use_case.get_by_id(user.id)
            return User(**res.dict())
        except exc.BadRequest as e:
            raise HTTPException(
                detail="user not found",
                status_code=HTTPStatus.NOT_FOUND,
            ) from e
