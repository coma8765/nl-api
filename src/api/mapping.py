"""Overrides mapping for API"""
from typing import Any, Callable

API_OVERRIDES: dict[Callable[..., Any], Callable[..., Any]] = {}
