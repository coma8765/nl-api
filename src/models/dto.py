"""DTO models"""
from typing import Generic, TypeVar

from pydantic.generics import GenericModel

from src.core.pydantic import BaseModel

DataT = TypeVar("DataT", bound=BaseModel)


class List(GenericModel, Generic[DataT]):
    list: list[DataT]
    page: int
    has_next: bool
