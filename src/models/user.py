"""Models for users"""
from __future__ import annotations

import uuid
from datetime import datetime
from uuid import UUID

from src.core.pydantic import BaseModel


class UserRef(BaseModel):
    """Reference for create user"""

    login: str
    name: str
    surname: str

    @property
    def full(self) -> User:
        """Creates user

        Returns:
            User: Full user model
        """
        assert getattr(self, "id", None) is None

        return User(
            id=uuid.uuid4(),
            created_at=datetime.utcnow(),
            **self.dict(),
        )


class User(UserRef):
    """Full user model"""

    id: UUID
    created_at: datetime
    hashed_password: str | None
