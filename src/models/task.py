"""Models for users"""
from __future__ import annotations

import uuid
from datetime import datetime
from enum import StrEnum, auto
from uuid import UUID

from pydantic import validator

from src.core.pydantic import BaseModel


class TaskStatus(StrEnum):
    """Task statuses"""

    CREATED = auto()
    IN_PROCESS = auto()
    DONE = auto()


class TaskRef(BaseModel):
    """Task reference"""

    title: str
    description: str = ""
    status: TaskStatus = TaskStatus.CREATED
    deadline: datetime | None

    def create(self, user_id: UUID) -> Task:
        """Creates full Task model

        Returns:
            Task: generated task
        """
        return Task(
            **self.dict(),
            id=uuid.uuid4(),
            user_id=user_id,
            created_at=datetime.utcnow(),
        )


class TaskUpd(BaseModel):
    """Task reference for update"""

    title: str | None = None
    description: str | None = None
    status: TaskStatus | None = None
    deadline: datetime | None = None

    def create(self, user_id: UUID) -> Task:
        """Creates full Task model

        Returns:
            Task: generated task
        """
        return Task(
            **self.dict(),
            id=uuid.uuid4(),
            user_id=user_id,
            created_at=datetime.utcnow(),
        )


class Task(TaskRef):
    """Full task model"""

    id: UUID

    user_id: UUID
    created_at: datetime


class TaskFilter(BaseModel):
    status: list[TaskStatus]
