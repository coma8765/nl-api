"""Users"""
import uuid

import sqlalchemy as sa
from fastapi import Depends
from sqlalchemy import Result
from sqlalchemy.exc import NoResultFound

from src.core import exc
from src.db.schemes.user import User
from src.db.sqlalchemy import Session, get_db
from src.models import user as models
from src.services.user import LoginNonUnique, UserRepository


class UserPGRepository(UserRepository):
    """Postgres user repository"""

    def __init__(self, session: Session = Depends(get_db)):
        self._session = session

    async def get_by_id(self, user_id: uuid.UUID) -> models.User:
        """Returns user by id using persistence

        Args:
            user_id: ID of user

        Returns:
            User with some ID

        Raises:
            NotFount: If user doesn't found
        """
        sa.select()
        stm = sa.select(User).where(User.id == user_id)
        res: Result = await self._session.execute(stm)
        try:
            return models.User.from_orm(res.one()[0])
        except NoResultFound as e:
            raise exc.NotFound from e

    async def get_by_login(
        self,
        login: str,
    ) -> models.User:
        """Returns user by login using persistence

        Returns:
            User: User with some login
        Raises:
            NotFound: If user with some login doesn't found
        """
        stm = sa.select(User).where(User.login == login)
        res: Result = await self._session.execute(stm)
        try:
            return models.User.from_orm(res.one()[0])
        except NoResultFound as e:
            raise exc.NotFound from e

    async def save(self, user_ref: models.User) -> None:
        """Save user to persistence"""
        try:
            await self.get_by_login(user_ref.login)
            raise LoginNonUnique(f"User with login {user_ref.login} found")
        except exc.NotFound:
            self._session.add(User(**user_ref.dict()))
