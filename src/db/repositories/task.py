"""Task postgres repository"""
import uuid

from fastapi import Depends
from sqlalchemy import Result, select
from sqlalchemy.exc import NoResultFound

from src.core import exc
from src.db.schemes.task import Task
from src.db.sqlalchemy import Session, get_db
from src.models import task as models
from src.models.dto import List
from src.models.task import TaskFilter
from src.services.task import TaskRepository

LIST_SIZE = 10


class TaskPGRepository(TaskRepository):
    """Task Postgres repository"""

    def __init__(self, session: Session = Depends(get_db)):
        self._session = session

    async def get_by_id(self, task_id: uuid.UUID) -> models.Task:
        """Returns task by ID using persistence postgres

        Returns:
            Task: Task with some ID

        Raises:
             NotFound: If task with some ID not found
        """
        stmt = select(Task).where(Task.id == task_id)
        res: Result = await self._session.execute(stmt)

        try:
            return models.Task.from_orm(res.one()[0])
        except NoResultFound as e:
            raise exc.NotFound from e

    async def get_user_notes(
        self,
        user_id: uuid.UUID,
        page: int = 1,
        params: TaskFilter | None = None,
    ) -> List[models.Task]:
        """Returns tasks by owner ID using persistence postgres

        Returns:
            List: List of notes with some owner
        """
        stmt = select(Task)
        stmt = stmt.where(Task.user_id == user_id)

        if params is not None and len(params.status) != 0:
            # noinspection PyProtectedMember
            stmt = stmt.where(
                Task._status.in_([i.value for i in params.status]),
            )

        stmt = stmt.order_by(Task.created_at)
        stmt = stmt.limit(LIST_SIZE + 1)
        stmt = stmt.offset(LIST_SIZE * (page - 1))

        res = await self._session.execute(stmt)
        data = res.all()

        return List(
            list=[models.Task.from_orm(i[0]) for i in data[:LIST_SIZE]],
            has_next=len(data) == LIST_SIZE + 1,
            page=page,
        )

    async def save(self, ref: models.TaskRef) -> None:
        """Saves task with some ref using persistence postgres"""
        task = Task(**ref.dict())
        self._session.add(task)

    async def update(self, task_id: uuid.UUID, ref: models.TaskUpd) -> None:
        """Updates task using persistence postgres

        Raises:
            NotFound: If task not found
        """
        stmt = select(Task).where(Task.id == task_id)
        task = (await self._session.execute(stmt)).one()[0]

        for name, key in ref.dict(exclude_unset=True, exclude_none=True).items():
            setattr(task, name, key)

        self._session.add(task)
