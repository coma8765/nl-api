"""Postgres adapter"""
from functools import lru_cache
from typing import Any, Callable

import yarl
from asyncpg import Pool, create_pool
from asyncpg.pool import PoolConnectionProxy
from fastapi import Depends

from src.core.exc import NotInitException

PGConnection = PoolConnectionProxy


class PGPool:
    """Postgres client"""

    __slots__ = ("_pool",)

    _pool: Pool | None

    def __init__(self) -> None:
        self._pool = None

    async def async_init(self, dns: yarl.URL) -> None:
        """Async init postgres pool"""

        self._pool = await create_pool(str(dns))

    async def async_close(self) -> None:
        """Async close postgres pool"""
        if self._pool is None:
            raise NotInitException()

        await self._pool.close()

    async def __anext__(self) -> PGConnection:
        if self._pool is None:
            raise NotInitException()

        async with self._pool.acquire() as connection:
            async with connection.transaction():
                return connection


@lru_cache()
def get_pg_pool() -> PGPool:
    """Get Postgres database

    Returns:
        PGPool: Postgres connection pool
    """
    return PGPool()


async def get_pg(pool: PGPool = Depends()) -> PGConnection:
    """Get Postgres connection

    Returns:
        PGConnection: Postgres connection from pool
    """
    return await anext(pool)


POSTGRES_OVERRIDES: dict[Callable[..., Any], Callable[..., Any]] = {
    PGPool: get_pg_pool,
}
