"""Adapter overrides"""
from abc import ABC
from typing import Any, Callable, Type

from src.db.repositories.task import TaskPGRepository
from src.db.repositories.user import UserPGRepository
from src.services.task import TaskRepository
from src.services.user import UserRepository

DB_OVERRIDES: dict[Type[ABC] | Callable[..., Any], Type[ABC] | Callable[..., Any]] = {
    UserRepository: UserPGRepository,
    TaskRepository: TaskPGRepository,
}
