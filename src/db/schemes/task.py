"""Task SQLAlchemy schemas"""
import uuid
from datetime import datetime
from uuid import UUID

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.schemes.base import Base
from src.db.schemes.user import User
from src.models.task import TaskStatus


class Task(Base):
    """Note SQLAlchemy scheme"""

    __tablename__ = "tasks"

    id: Mapped[UUID] = mapped_column(primary_key=True, default=uuid.uuid4)

    title: Mapped[str] = mapped_column(nullable=False)
    description: Mapped[str] = mapped_column(nullable=False)
    _status: Mapped[str] = mapped_column(nullable=False)
    deadline: Mapped[datetime] = mapped_column(nullable=True)

    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("users.id"),
        nullable=False,
    )
    user: Mapped[list[User]] = relationship("User", back_populates="tasks")

    created_at: Mapped[datetime] = mapped_column(nullable=False)

    @property
    def status(self) -> TaskStatus:
        """Returns status of task

        Returns:
            TaskStatus: Status of task
        """
        return TaskStatus(self._status)

    @status.setter
    def status(self, value: TaskStatus | None) -> None:
        if value is not None:
            self._status: str = str(value.value)
