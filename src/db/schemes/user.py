"""User SQLAlchemy schemas"""
import uuid
from datetime import datetime

from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.schemes.base import Base


class User(Base):
    """User SQLAlchemy scheme"""

    __tablename__ = "users"

    id: Mapped[uuid.UUID] = mapped_column(
        primary_key=True,
        default=uuid.uuid4,
    )

    login: Mapped[str] = mapped_column(nullable=False, unique=True)
    name: Mapped[str] = mapped_column(nullable=False)
    surname: Mapped[str] = mapped_column(nullable=False)
    hashed_password: Mapped[str] = mapped_column(nullable=False)

    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    tasks = relationship("Task", back_populates="user")
