"""SQLAlchemy configs"""
from functools import lru_cache
from typing import Any, AsyncGenerator, Callable

import yarl
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine

from src.core.config import POSTGRES_DNS

Session = AsyncSession


class SQLAlchemy:
    """SQLAlchemy"""

    engine: AsyncEngine

    def __init__(self, dns: str):
        if isinstance(dns, str):
            dns_ = yarl.URL(dns)
        elif isinstance(dns, yarl.URL):
            dns_ = dns
        else:
            raise ValueError("DNS may only str or yarl.URL")

        if dns_.scheme == "postgres":
            dns_ = dns_.with_scheme("postgresql+asyncpg")

        self.engine = create_async_engine(
            str(dns_),
            echo=True,
        ).execution_options(
            max_size=10,
            max_inactive_connection_lifetime=100,
        )

    async def create_tables(self) -> None:
        # noinspection PyUnresolvedReferences
        import src.main
        from src.db.schemes.base import Base

        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)


@lru_cache()
def get_sql_alchemy(dns: str | None = None) -> SQLAlchemy:
    """Returns SQLAlchemy

    Returns:
        SQLAlchemy: SQLAlchemy instance
    """
    if dns is None:
        dns = str(POSTGRES_DNS)

    return SQLAlchemy(dns)


async def get_db() -> AsyncGenerator[AsyncSession, None]:
    """Returns SQLAlchemy Session"""
    alchemy = get_sql_alchemy()
    async with Session(
        alchemy.engine,
        expire_on_commit=False,
    ) as conn:
        async with conn.begin():
            yield conn
            await conn.flush()
            await conn.commit()

    return


SQLALCHEMY_OVERRIDES: dict[Callable[..., Any], Callable[..., Any]] = {
    SQLAlchemy: get_sql_alchemy,
    Session: get_db,
}


async def migrate(dns: str | yarl.URL = POSTGRES_DNS) -> None:
    alchemy = SQLAlchemy(str(dns))
    await alchemy.create_tables()


if __name__ == "__main__":
    import asyncio

    asyncio.run(migrate())
