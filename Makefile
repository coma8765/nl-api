help:
	echo 'look README.md'

dev:
	pipenv run python -m src.main

start:
	pipenv run uvicorn src.main:app --host 0.0.0.0 --port 8000

install:
	pipenv install

install-dev:
	pipenv install --dev

lint:
	pipenv run isort	src
	pipenv run black	src
	pipenv run pylint	src	--jobs $(shell nproc)
	pipenv run mypy		src

migrate:
	pipenv run python -m src.db.sqlalchemy


build:
	docker build -f templates/Dockerfile -t nl-api .
